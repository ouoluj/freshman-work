//****************************************************************
//
//  Class:   Mancala
//
//  Purpose: Implementation for the
//
//
//****************************************************************
#include "mancala.h"
#include "game.h"
#include <iostream>

using namespace std;
namespace main_savitch_14 {

Mancala::Mancala() {
  board[0][0] = board [1][0] = 0;
  for (size_t i = 1; i < 7; i++) {
    board[0][i] = board[1][i] = 4;
  }
}

game* Mancala::clone()const {return new Mancala(*this);}

void Mancala::compute_moves(queue<string>& moves)const {}

void Mancala::make_move(const std::string& move) {
  if (!is_legal(move)) {return;}


  int row;
  if (next_mover() == HUMAN)
    {row = 1;}
  else
    {row = 0;}
  int cup = (move[0] - '0');
  int gemNumber = board[row][cup];
  bool freeMove = false;


  board[row][cup] = 0;
  if (next_mover() == HUMAN)
    {cup++;}
  else
    {cup--;}

  for (int i = 0; i < gemNumber; i++) {
    if (next_mover() == HUMAN) {
      if (cup == 7) {
        board[row][0]++;
        cup = 6;
        row = 0;

        if (i == gemNumber - 1) {
          freeMove = true;
        }
      }
      else if (cup == 0) {
        row = 1;
        cup = 2;
        board[row][1]++;
        if (i == gemNumber-1 && board[1][1] == 1 && board[0][1] != 0) {
          board[1][0] += board[0][1] + 1;
          board[0][1] = board[1][1] = 0;
        }
      }
      else {
        if (row == 0) {
          board[row][cup]++;
          cup--;
        }

        if (row == 1) {
          if (i == gemNumber-1 && board[row][cup] == 0 && board[0][cup] != 0) {
            board[1][0] += board[0][cup] + 1;
            board[0][cup] = 0;
          }
          else {
            board[row][cup]++;
            cup++;
          }
        }
      }
    } // End of Human turn

    else {
      if (cup == 0) {
        board[row][0]++;
        cup = 1;
        row = 1;

        if (i == gemNumber - 1) {
          freeMove = true;
        }
      }
      else if (cup == 7) {
        row = 0;
        cup = 6;
        board[row][6]++;
        if (i == gemNumber-1 && board[0][6] == 1 && board[1][6] != 0) {
          board[0][0] += board[1][6] + 1;
          board[0][6] = board[1][6] = 0;
        }
      }
      else {
        if (row == 0) {
          if (i == gemNumber-1 && board[row][cup] == 0 && board[1][cup] != 0) {
            board[0][0] += board[1][cup] + 1;
            board[1][cup] = 0;
          }
          else {
            board[row][cup]++;
            cup--;
          }
        }

        if (row == 1) {
          board[row][cup]++;
          cup++;
        }
      }
    } // End of Computer turn
  } // End of for loop

  if (freeMove == true) {
    cout << "Free move!" << endl;
    freeMove = false;
  }
  else
    {game::make_move(move);}

  if (is_game_over()) {
    display_status();
    for (int i = 1; i < 7; i++) {
      board[0][0] += board [0][i];
      board[0][i] = 0;
      board[1][0] += board [1][i];
      board[1][i] = 0;
    }
    cout << "Game is over!" << endl;
  }
}

bool Mancala::is_game_over()const{
  bool p1 = true;
  bool p2 = true;
  for (size_t i = 1; i < 7; i++) {
    if (board[0][i] > 0) {p1 = false;}
    if (board[1][i] > 0) {p2 = false;}
  }
  return (p1 || p2);
}

game::who Mancala::winning(game::who player)const {
  if (board[0][0] > board[1][0])
    {return COMPUTER;}
  else if (board[0][0] < board[1][0])
    {return HUMAN;}
  else
    {return NEUTRAL;}
}

void Mancala::display_status()const {
  cout << endl;
  cout << GREEN << "   |";
  for (size_t i = 1; i < 7; i++) {
    cout << board[0][i] << "|";
  }
  cout << endl;
  cout << "  " << RED << board[0][0] << "             " << CYAN << board[1][0] << endl;
  cout << GREEN << "   |";
  for (size_t i = 1; i < 7; i++) {
    cout << board[1][i] << "|";
  }
  cout << endl << endl;
}

int Mancala::evaluate()const {return board[0][0] - board[1][0];}

bool Mancala::is_legal(const string& move)const {
  int cup = (move[0] - '0');
  int row;
  if (next_mover() == HUMAN)
    {row = 1;}
  else
    {row = 0;}
  return cup <= 6 && cup >= 1 && board[row][cup] != 0;
}

} // End of namespace main_savitch_14
