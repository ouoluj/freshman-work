/*****************************************************
This is a sample of what the main should like for the first phase of the
Mancala game.
	John Dolan	Ohio University		Spring 2019
*************************************************************/
#include<iostream>
#include "mancala.h"
using namespace std;
using namespace main_savitch_14;


int main(){
    Mancala stonegame;
   stonegame.play();

return 0;
}

