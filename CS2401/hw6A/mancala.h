//****************************************************************
//
//  Class:   Mancala
//
//  Purpose: Header file of the mancala class. Child of the game class.
//
//****************************************************************
#ifndef MANCALA_H
#define MANCALA_H
#include "game.h"
#include "colors.h"
namespace main_savitch_14 {

class Mancala : public game {
public:
  Mancala();
  game* clone()const;
  void compute_moves(std::queue<std::string>& moves)const;
  void make_move(const std::string& move);
  bool is_legal(const std::string& move)const;
  int evaluate()const;
  bool is_game_over()const;
  // game::who winning(game::who player)const;
  void display_status()const;

private:
  int board[2][7];
};

} // End of namespace main_savitch_14
#endif
