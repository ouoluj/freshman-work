// Ignore :V
#include <iostream>
#include <fstream>
#include <list>
// #include <iterator>
// #include "home.h"
#include "wolfHome.h"
#include "dragonHome.h"
#include "hawkHome.h"
using namespace std;

// int menu();
void show_list(ostream& outs, list<Home*> &homes);

int main() {
  // ifstream ins;
  ofstream outs;
  // ins.open("database.txt");

  list<Home*> homes;
  Home *test;
  test = new wolfHome;
  test->input(std::cin);
  homes.push_back(test);
  // wolfHome *test = new wolfHome;

  test = new dragonHome;
  test->input(std::cin);
  homes.push_back(test);

  test = new hawkHome;
  test->input(std::cin);
  homes.push_back(test);



  // test->output(std::cout);
  // homes.push_back(test);
  show_list(cout, homes);


  outs.open("database.txt");
  show_list(outs, homes);
  return 0;
}

// int menu() {
//   int tmp = 0;
//   cout << "What kind of house would you like to build?" << endl;
//   cout << " 1: Wolf" << endl;
//   cout << " 2: Dragon" << endl;
//   cout << " 3: Hawk" << endl;
//   cout << " 4: aaa" << endl;
//   cout << " 5: aaa" << endl;
//   cout << "Enter 6 to see all the homes that have been ordered" << endl;
//   cout << "Enter 9 to exit the program" << endl;
//   cin >> tmp;
//   return tmp;
// }

void show_list(ostream& outs, list<Home*> &homes) {
  list<Home*>::iterator it;
  for (it = homes.begin(); it != homes.end(); it++) {
    (*it)->output(outs);
  }
}
