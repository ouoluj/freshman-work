//****************************************************************
//
//  Class:   Home
//
//  Purpose: The implementation file for all of the Home classes being used.
//
//
//****************************************************************
#include <iostream>
#include "home.h"
#include "wolfHome.h"
#include "dragonHome.h"
#include "hawkHome.h"
#include "possumHome.h"
#include "snakeHome.h"

// Wolf
wolfHome::wolfHome() {
  width = 0;
  length = 0;
  occupancy = 0;
}

void wolfHome::input(std::istream& ins) {
  if (&std::cin == &ins) {
    std::cout << "Enter the width of the territory: ";
    ins >> width;
    std::cout << "Enter the length of the territory: ";
    ins >> length;
    std::cout << "Enter the number of wolves to live here: ";
    ins >> occupancy;
  }
  else {
    ins >> width;
    ins >> length;
    ins >> occupancy;
    ins.ignore(100, '\n');
  }
}

void wolfHome::output(std::ostream& outs) {
  if (&std::cout == &outs) {
    outs << "Wolf" << '\n';
    outs << "Width of the territory:  " << width << '\n';
    outs << "Length of the territory: " << length << '\n';
    outs << "Number of wolves here:   " << occupancy << '\n';
  }
  else {
    outs << "wolf" << '\n';
    outs << width << '\n';
    outs << length << '\n';
    outs << occupancy << '\n';
  }
}




// Dragon
dragonHome::dragonHome() {
  diameter = 0;
  height = 0;
  value = 0;
}

void dragonHome::input(std::istream& ins) {
  if (&std::cin == &ins) {
    std::cout << "Enter the diameter of the cave: ";
    ins >> diameter;
    std::cout << "Enter the height of the cave: ";
    ins >> height;
    std::cout << "Enter the value of the hoard: ";
    ins >> value;
  }
  else {
    ins >> diameter;
    ins >> height;
    ins >> value;
    ins.ignore(100, '\n');
  }
}

void dragonHome::output(std::ostream& outs) {
  if (&std::cout == &outs) {
    outs << "Dragon" << '\n';
    outs << "Diameter of the cave: " << diameter << '\n';
    outs << "Height of the cave:   " << height << '\n';
    outs << "Value of the hoard:   " << value << '\n';
  }
  else {
    outs << "dragon" << '\n';
    outs << diameter << '\n';
    outs << height << '\n';
    outs << value << '\n';
  }
}




// Hawk
hawkHome::hawkHome() {
  type = "";
  height = 0;
  diameter = 0;
}

void hawkHome::input(std::istream& ins) {
  if (&std::cin == &ins) {
    std::cout << "Enter the type of the tree: ";
    while(std::cin.peek() == '\n' || std::cin.peek() == '\r')
    {std::cin.ignore();}
    getline(ins, type);
    std::cout << "Enter the height of the tree: ";
    ins >> height;
    std::cout << "Enter the diameter of the nest: ";
    ins >> diameter;
  }
  else {
    getline(ins, type);
    ins >> height;
    ins >> diameter;
    ins.ignore(100, '\n');
  }
}

void hawkHome::output(std::ostream& outs) {
  if (&std::cout == &outs) {
    outs << "Hawk" << '\n';
    outs << "Type of the tree:     " << type << '\n';
    outs << "Height of the tree:   " << height << '\n';
    outs << "Diameter of the nest: " << diameter << '\n';
  }
  else {
    outs << "hawk" << '\n';
    outs << type << '\n';
    outs << height << '\n';
    outs << diameter << '\n';
  }
}




// Possum
possumHome::possumHome() {
  type = "";
  capacity = 0;
  diameter = 0;
}

void possumHome::input(std::istream& ins) {
  if (&std::cin == &ins) {
    std::cout << "Enter the type of trash can: ";
    while(std::cin.peek() == '\n' || std::cin.peek() == '\r')
    {std::cin.ignore();}
    getline(ins, type);
    std::cout << "Enter the capacity of the trash can (in gallons): ";
    ins >> capacity;
    std::cout << "Enter the diameter of the can's entrance: ";
    ins >> diameter;
  }
  else {
    getline(ins, type);
    ins >> capacity;
    ins >> diameter;
    ins.ignore(100, '\n');
  }
}

void possumHome::output(std::ostream& outs) {
  if (&std::cout == &outs) {
    outs << "Possum" << '\n';
    outs << "Type of trash can:    " << type << '\n';
    outs << "Capacity of can:      " << capacity << '\n';
    outs << "Diameter of entrance: " << diameter << '\n';
  }
  else {
    outs << "possum" << '\n';
    outs << type << '\n';
    outs << capacity << '\n';
    outs << diameter << '\n';
  }
}




// Snake
snakeHome::snakeHome() {
  venomous = 0;
  depth = 0;
  temp = 0.0;
}

void snakeHome::input(std::istream& ins) {
  if (&std::cin == &ins) {
    std::cout << "Is this snake venomous? 1 - Yes, 0 - No: ";
    ins >> venomous;
    std::cout << "Enter the depth of the hole: ";
    ins >> depth;
    std::cout << "Enter the temperature of the dirt: ";
    ins >> temp;
  }
  else {
    ins >> venomous;
    ins >> depth;
    ins >> temp;
    ins.ignore(100, '\n');
  }
}

void snakeHome::output(std::ostream& outs) {
  if (&std::cout == &outs) {
    outs << "Snake" << '\n';
    outs << "Venomosity:          " << venomous << '\n';
    outs << "Depth of hole:       " << depth << '\n';
    outs << "Temperature of dirt: " << temp << '\n';
  }
  else {
    outs << "snake" << '\n';
    outs << venomous << '\n';
    outs << depth << '\n';
    outs << temp << '\n';
  }
}
