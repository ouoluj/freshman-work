//****************************************************************
//
//  Class:   Home
//
//  Purpose: The abstract class for animal homes, used to hold the homes of
//          various different animals.
//
//****************************************************************
#ifndef HOME_H
#define HOME_H
#include <iostream>

class Home {
public:
  virtual void input(std::istream& ins) = 0;
  virtual void output(std::ostream& outs) = 0;
};
#endif
