//****************************************************************
//  Name:    Derek Baker
//  Class:   dnode
//
//  Purpose: A header for a class of nodes, suitable for use in a doubly-linked
//        list.
//
//****************************************************************

#ifndef DNODE_H
#define DNODE_H

template <class N>
class dnode {

public:
  dnode(N item = N(), dnode *pptr = NULL, dnode *nptr = NULL)
    {datafield = item;
     prevlink = pptr;
     nextlink = nptr;}

  N data() {return datafield;}
  dnode* previous() {return prevlink;}
  dnode* next() {return nextlink;}

  void set_data(N d) {datafield = d;}
  void set_previous(dnode *ptr) {prevlink = ptr;}
  void set_next(dnode *ptr) {nextlink = ptr;}

private:
  N datafield;
  dnode *nextlink;
  dnode *prevlink;
};
#endif
